<?php

namespace Drupal\commerce_rajaongkir\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RajaongkirApiKeyForm.
 */
class RajaongkirApiKeyForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_rajaongkir.rajaongkirapikey',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rajaongkir_api_key_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_rajaongkir.rajaongkirapikey');
    $form['your_rajaongkir_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your Rajaongkir PRO API key'),
      '#description' => $this->t('Enter your Rajaongkir PRO API key'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('your_rajaongkir_api_key'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('commerce_rajaongkir.rajaongkirapikey')
      ->set('your_rajaongkir_api_key', $form_state->getValue('your_rajaongkir_api_key'))
      ->save();
  }

}

<?php

namespace Drupal\commerce_rajaongkir\Controller;

use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\RequestException;

/**
 * Class TrackingController.
 */
class TrackingController extends ControllerBase {

  /**
   * Viewshipment.
   *
   * @return string
   *   Return Hello string.
   */
  public function viewShipment($courier, $tracking_number) {
    $config = \Drupal::config('commerce_rajaongkir.rajaongkirapikey');
    $api_key = $config->get('your_rajaongkir_api_key');

    $client = \Drupal::httpClient();

    try {
      $request = $client->post("https://pro.rajaongkir.com/api/waybill", [
        'form_params' => [
          'waybill' => $tracking_number,
          'courier' => $courier,
        ],
        'headers' => [
          'key' => $api_key,
        ],
      ]);
      $arrayResponse = json_decode($request->getBody(), TRUE);

      $result = $arrayResponse['rajaongkir']['result'];

      return [
        '#theme' => 'tracking',
        '#status' => $result['delivery_status']['status'],
        '#waybill_number' => $tracking_number,
        '#service_code' => $result['summary']['service_code'],
        '#waybill_date' => $result['summary']['waybill_date'],
        '#weight' => $result['details']['weight'],
        '#shipper_name' => $result['details']['shippper_name'],
        '#shipper_city' => $result['details']['shipper_city'],
        '#receiver_name' => $result['details']['receiver_name'],
        '#receiver_address1' => $result['details']['receiver_address1'],
        '#receiver_address2' => $result['details']['receiver_address2'],
        '#receiver_address3' => $result['details']['receiver_address3'],
        '#receiver_city' => $result['details']['receiver_city'],
        '#manifest' => $result['manifest'],
        '#valid' => TRUE,
      ];
    } catch (RequestException $e) {
      return [
        '#theme' => 'tracking',
        '#status' => 'INVALID NUMBER',
        '#waybill_number' => $tracking_number,
        '#valid' => FALSE,
      ];
    }
  }

}

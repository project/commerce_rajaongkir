<?php

namespace Drupal\commerce_rajaongkir;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;

class Rajaongakir {

  /**
   * Rajaongkir::$apiKey
   *
   * Rajaongkir API key.
   *
   * @access  protected
   * @type    string
   */
  protected $apiKey = NULL;

  /**
   * Supported Couriers
   *
   * @access  protected
   * @type    array
   */
  protected $supportedCouriers = [
    'jne',
    'pos',
    'tiki',
    'rpx',
    'esl',
    'pcp',
    'pandu',
    'wahana',
    'sicepat',
    'jnt',
    'pahala',
    'cahaya',
    'sap',
    'jet',
    'indah',
    'slis',
    'expedito*',
    'dse',
    'first',
    'ncs',
    'star',
    'lion',
    'ninja-express',
    'idl',
    'rex',
  ];

  /**
   * Rajaongkir::$couriersList
   *
   * Rajaongkir courier list.
   *
   * @access  protected
   * @type array
   */
  protected $couriersList = [
    'jne' => 'Jalur Nugraha Ekakurir (JNE)',
    'pos' => 'POS Indonesia (POS)',
    'tiki' => 'Citra Van Titipan Kilat (TIKI)',
    'pcp' => 'Priority Cargo and Package (PCP)',
    'esl' => 'Eka Sari Lorena (ESL)',
    'rpx' => 'RPX Holding (RPX)',
    'pandu' => 'Pandu Logistics (PANDU)',
    'wahana' => 'Wahana Prestasi Logistik (WAHANA)',
    'sicepat' => 'SiCepat Express (SICEPAT)',
    'j&t' => 'J&T Express (J&T)',
    'pahala' => 'Pahala Kencana Express (PAHALA)',
    'cahaya' => 'Cahaya Logistik (CAHAYA)',
    'sap' => 'SAP Express (SAP)',
    'jet' => 'JET Express (JET)',
    'indah' => 'Indah Logistic (INDAH)',
    'slis' => 'Solusi Express (SLIS)',
    'expedito*' => 'Expedito*',
    'dse' => '21 Express (DSE)',
    'first' => 'First Logistics (FIRST)',
    'ncs' => 'Nusantara Card Semesta (NCS)',
    'star' => 'Star Cargo (STAR)',
  ];

  /**
   * Rajaongkir::$supportedWaybills
   *
   * Rajaongkir supported couriers waybills.
   *
   * @access  protected
   * @type    array
   */
  protected $supportedWayBills = [
    'jne',
    'pos',
    'tiki',
    'pcp',
    'rpx',
    'wahana',
    'sicepat',
    'j&t',
    'sap',
    'jet',
    'dse',
    'first',
  ];

  protected $countries = [
    'AD' => 189,
    'AE' => 178,
    'AF' => 2,
    'AG' => 7,
    'AI' => '',
    'AL' => 4,
    'AM' => 190,
    'AN' => 126,
    'AO' => 6,
    'AQ' => '',
    'AR' => 8,
    'AS' => 185,
    'AT' => 10,
    'AU' => 9,
    'AW' => 191,
    'AX' => '',
    'AZ' => 11,
    'BA' => 194,
    'BB' => 15,
    'BD' => 14,
    'BE' => 16,
    'BF' => 26,
    'BG' => 25,
    'BH' => 13,
    'BI' => 27,
    'BJ' => 18,
    'BL' => 221,
    'BM' => 19,
    'BN' => 24,
    'BO' => 21,
    'BQ' => '',
    'BR' => 23,
    'BS' => 12,
    'BT' => 20,
    'BV' => '',
    'BW' => 22,
    'BY' => 192,
    'BZ' => 17,
    'CA' => 30,
    'CC' => '',
    'CD' => 235,
    'CF' => 33,
    'CG' => 39,
    'CH' => 163,
    'CI' => 42,
    'CK' => 40,
    'CL' => 35,
    'CM' => 29,
    'CN' => 36,
    'CO' => 37,
    'CR' => 41,
    'CU' => 43,
    'CV' => 196,
    'CW' => 199,
    'CX' => '',
    'CY' => 44,
    'CZ' => 45,
    'DE' => 66,
    'DJ' => 48,
    'DK' => 47,
    'DM' => 49,
    'DO' => 50,
    'DZ' => 5,
    'EC' => 51,
    'EE' => 200,
    'EG' => 52,
    'EH' => '',
    'ER' => 55,
    'ES' => 156,
    'ET' => 56,
    'FI' => 59,
    'FJ' => 58,
    'FK' => 202,
    'FM' => '',
    'FO' => 57,
    'FR' => 61,
    'GA' => 64,
    'GB' => 69,
    'GD' => 72,
    'GE' => 203,
    'GF' => 62,
    'GG' => 204,
    'GH' => 67,
    'GI' => 68,
    'GL' => 71,
    'GM' => 65,
    'GN' => 76,
    'GP' => 73,
    'GQ' => 54,
    'GR' => 70,
    'GS' => '',
    'GT' => 75,
    'GU' => 74,
    'GW' => 77,
    'GY' => 78,
    'HK' => 81,
    'HM' => '',
    'HN' => 80,
    'HR' => 198,
    'HT' => 79,
    'HU' => 82,
    'ID' => '',
    'IE' => 87,
    'IL' => 88,
    'IM' => '',
    'IN' => 84,
    'IO' => '',
    'IQ' => 86,
    'IR' => 85,
    'IS' => 83,
    'IT' => 89,
    'JE' => 205,
    'JM' => 90,
    'JO' => 92,
    'JP' => 91,
    'KE' => 94,
    'KG' => '',
    'KH' => 28,
    'KI' => 95,
    'KM' => 38,
    'KN' => 223,
    'KP' => 46,
    'KR' => 96,
    'KW' => 97,
    'KY' => 31,
    'KZ' => 93,
    'LA' => 98,
    'LB' => 99,
    'LC' => 224,
    'LI' => 208,
    'LK' => 157,
    'LR' => 101,
    'LS' => 100,
    'LT' => 209,
    'LU' => 103,
    'LV' => 207,
    'LY' => 102,
    'MA' => 119,
    'MC' => 211,
    'MD' => 210,
    'ME' => 212,
    'MF' => 225,
    'MG' => 105,
    'MH' => '',
    'MK' => 60,
    'ML' => 110,
    'MM' => 121,
    'MN' => 117,
    'MO' => 104,
    'MP' => 112,
    'MQ' => 113,
    'MR' => 114,
    'MS' => 118,
    'MT' => 111,
    'MU' => 115,
    'MV' => 109,
    'MW' => 107,
    'MX' => 116,
    'MY' => 108,
    'MZ' => 120,
    'NA' => 122,
    'NC' => 127,
    'NE' => 130,
    'NF' => '',
    'NG' => 131,
    'NI' => 129,
    'NL' => 125,
    'NO' => 132,
    'NP' => 124,
    'NR' => 123,
    'NU' => 214,
    'NZ' => 128,
    'OM' => 133,
    'PA' => 135,
    'PE' => 138,
    'PF' => 63,
    'PG' => 136,
    'PH' => 139,
    'PK' => 134,
    'PL' => 140,
    'PM' => '',
    'PN' => '',
    'PR' => 215,
    'PS' => '',
    'PT' => 141,
    'PW' => '',
    'PY' => 137,
    'QA' => 142,
    'RE' => 216,
    'RO' => 143,
    'RS' => 218,
    'RU' => 144,
    'RW' => 145,
    'SA' => 147,
    'SB' => 154,
    'SC' => 150,
    'SD' => 159,
    'SE' => 162,
    'SG' => 152,
    'SH' => 158,
    'SI' => 219,
    'SJ' => '',
    'SK' => 153,
    'SL' => 151,
    'SM' => '',
    'SN' => 149,
    'SO' => 220,
    'SR' => 160,
    'SS' => 159,
    'ST' => '',
    'SV' => 53,
    'SX' => 225,
    'SY' => 164,
    'SZ' => 161,
    'TC' => 175,
    'TD' => 34,
    'TF' => '',
    'TG' => 169,
    'TH' => 167,
    'TJ' => 228,
    'TK' => '',
    'TL' => 168,
    'TM' => 229,
    'TN' => 173,
    'TO' => 170,
    'TR' => 174,
    'TT' => 172,
    'TV' => 176,
    'TW' => 165,
    'TZ' => 166,
    'UA' => 230,
    'UG' => 177,
    'UM' => '',
    'US' => 179,
    'UY' => 180,
    'UZ' => 231,
    'VA' => 182,
    'VC' => 226,
    'VE' => 183,
    'VG' => 232,
    'VI' => 233,
    'VN' => 184,
    'VU' => 181,
    'WF' => '',
    'WS' => 146,
    'YE' => 186,
    'YT' => '',
    'ZA' => 155,
    'ZM' => 187,
    'ZW' => 188,
  ];

  protected $province = [
    'Bali' => 1,
    'Bangka Belitung' => 2,
    'Banten' => 3,
    'Bengkulu' => 4,
    'DI Yogyakarta' => 5,
    'DKI Jakarta' => 6,
    'Gorontalo' => 7,
    'Jambi' => 8,
    'Jawa Barat' => 9,
    'Jawa Tengah' => 10,
    'Jawa Timur' => 11,
    'Kalimantan Barat' => 12,
    'Kalimantan Selatan' => 13,
    'Kalimantan Tengah' => 14,
    'Kalimantan Timur' => 15,
    'Kalimantan Utara' => 16,
    'Kepulauan Riau' => 17,
    'Lampung' => 18,
    'Maluku' => 19,
    'Maluku Utara' => 20,
    'Nanggroe Aceh Darussalam (NAD)' => 21,
    'Nusa Tenggara Barat (NTB)' => 22,
    'Nusa Tenggara Timur (NTT)' => 23,
    'Papua' => 24,
    'Papua Barat' => 25,
    'Riau' => 26,
    'Sulawesi Barat' => 27,
    'Sulawesi Selatan' => 28,
    'Sulawesi Tengah' => 29,
    'Sulawesi Tenggara' => 30,
    'Sulawesi Utara' => 31,
    'Sumatera Barat' => 32,
    'Sumatera Selatan' => 33,
    'Sumatera Utara' => 34,
  ];

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Rajaongkir::$response
   *
   * Rajaongkir response.
   *
   * @access  protected
   * @type    mixed
   */
  protected $response;

  /**
   * Rajaongkir::__construct
   *
   * @access  public
   * @throws  \InvalidArgumentException
   */
  public function __construct($apiKey) {
    $this->httpClient = \Drupal::httpClient();
    $this->apiKey = $apiKey;
  }


  /**
   * @return string
   */
  public function getApiKey() {
    return $this->apiKey;
  }

  /**
   * @param string $apiKey
   */
  public function setApiKey($apiKey) {
    $this->apiKey = $apiKey;
  }

  public function getProvinceCode($name){
    return $this->province[$name];
  }

  function getCityCode($city, $province) {
    $type = '';
    if (substr_count($city, '(Kota)') > 0) {
      $type = 'Kota';
      $city = substr($city, 0, -7);
    }
    if (substr_count($city, '(Kabupaten)') > 0) {
      $type = 'Kabupaten';
      $city = substr($city, 0, -12);
    }

    $client = \Drupal::httpClient();

    $request = $client->get("https://pro.rajaongkir.com/api/city?province=". $province, [
      'headers' => [
        'key' => $this->apiKey,
      ],
    ]);

    $arrayResponse = json_decode($request->getBody(), TRUE);
    $cities = $arrayResponse['rajaongkir']['results'];

    if ($type != '') {
      $keys = array_keys(array_column($cities, 'city_name'), $city);
      foreach ($keys as $k) {
        if ($cities[$k]['city_name'] == $city and $cities[$k]['type'] == $type) {
          $key = $k;
        }
      }
    } else {
      $key = array_search($city, array_column($cities, 'city_name'));
    }

    return $cities[$key]['city_id'];
  }

  function getKecamatanCode($kecamatan, $city) {
    $client = \Drupal::httpClient();

    $request = $client->get("https://pro.rajaongkir.com/api/subdistrict?city=". $city, [
      'headers' => [
        'key' => $this->apiKey,
      ],
    ]);

    $arrayResponse = json_decode($request->getBody(), TRUE);
    $cities = $arrayResponse['rajaongkir']['results'];

    $key = array_search($kecamatan, array_column($cities, 'subdistrict_name'));

    return $cities[$key]['subdistrict_id'];
  }

  /**
   * Returns an array of RajaOngkir country symbols keyed.
   */
  function getCountryCode($name) {
    return $this->countriescountries[$name];
  }

  /**
   * Rajaongkir::request
   *
   * Curl request API caller.
   *
   * @param string $path
   * @param array $params
   * @param string $type
   *
   * @access  protected
   * @return  array|bool Returns FALSE if failed.
   */
  protected function request($path, $params = [], $type = 'GET') {
    $apiUrl = 'https://pro.rajaongkir.com';

    $path = 'api/' . $path;
  }

}

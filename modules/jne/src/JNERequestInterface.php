<?php

namespace Drupal\commerce_rajaongkir_jne;

interface JNERequestInterface {

  /**
   * Set the request configuration.
   *
   * @param array $configuration
   *   A configuration array from a CommerceShippingMethod.
   */
  public function setConfig(array $configuration);

}

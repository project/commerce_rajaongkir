<?php

namespace Drupal\commerce_rajaongkir_jne\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\SupportsTrackingInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_rajaongkir_jne\JNERequestInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @CommerceShippingMethod(
 *  id = "jne",
 *  label = @Translation("JNE"),
 *  services = {
 *   "YES" = @translation("YES (Yakin Esok Sampai)"),
 *   "REG" = @translation("REG (Layanan Reguler)"),
 *   "OKE" = @translation("OKE (Ongkos Kirim Ekonomis)"),
 *   "SPS" = @translation("SPS (Super Speed)"),
 *   "CTC" = @translation("CTC (JNE City Courier)"),
 *   "CTCYES" = @translation("CTCYES (JNE City Courier)"),
 *   "CTCSPS" = @translation("CTCSPS (JNE City Courier)"),
 *   "PELIKAN" = @translation("PELIKAN"),
 *   "POPBOX" = @translation("POPBOX"),
 *   "JTR" = @translation("JTR (JNE Trucking)"),
 *   "JTR<150" = @translation("JTR<150 (JNE Trucking)"),
 *   "JTR250" = @translation("JTR250 (JNE Trucking)"),
 *   "JTR>250" = @translation("JTR>250 (JNE Trucking)"),
 *   "INTL" = @translation("INTL (International)"),
 *   },
 * )
 */
class JNE extends ShippingMethodBase implements SupportsTrackingInterface {
  /**
   * @var \Drupal\commerce_rajaongkir_jne\JNERateRequest
   */
  protected $jne_rate_service;

  /**
   * Constructs a new ShippingMethodBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $packageTypeManager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_rajaongkir_jne\JNERequestInterface $jne_rate_request
   *   The rate request service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $packageTypeManager, WorkflowManagerInterface $workflow_manager, JNERequestInterface $jne_rate_request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $packageTypeManager, $workflow_manager);

    $this->jne_rate_service = $jne_rate_request;
    $this->jne_rate_service->setConfig($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce_rajaongkir_jne.jne_rate_request')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_information' => [
        'access_key' => '',
        'user_id' => '',
        'password' => '',
        'mode' => 'test',
      ],
      'rate_options' => [
        'rate_type' => 0,
      ] ,
      'options' => [
        'tracking_url' => 'internal:/tracking/jne/[tracking_code]',
        'log' => [],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('JNE Options'),
      '#description' => $this->t('Additional options for JNE'),
    ];
    $form['options']['tracking_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tracking URL base'),
      '#description' => $this->t(
        'The base URL for assembling a tracking URL. If the [tracking_code]
         token is omitted, the code will be appended to the end of the URL
          (e.g. "https://wwwapps.ups.com/tracking/tracking.cgi?tracknum=123456789")'
      ),
      '#default_value' => $this->configuration['options']['tracking_url'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

    }
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Calculates rates for the given shipment.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The shipment.
   *
   * @return \Drupal\commerce_shipping\ShippingRate[]
   *   The rates.
   */
  public function calculateRates(ShipmentInterface $shipment) {
    $this->jne_rate_service->setShipment($shipment);
    return $this->jne_rate_service->getRates($this->parentEntity);
  }

    /**
   * Returns a tracking URL for JNE shipments.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   The commerce shipment.
   *
   * @return mixed
   *   The URL object or FALSE.
   */
  public function getTrackingUrl(ShipmentInterface $shipment) {
    $code = $shipment->getTrackingCode();

    if (!empty($code)) {
      // If the tracking code token exists, replace it with the code.
      if (strstr($this->configuration['options']['tracking_url'], '[tracking_code]')) {
        $url = str_replace('[tracking_code]', $code, $this->configuration['options']['tracking_url']);
        return Url::fromUri($url);
      }

      // Otherwise, append the tracking code to the end of the URL.
      $url = $this->configuration['options']['tracking_url'] . $code;
      return Url::fromUri($url);
    }

    return FALSE;
  }

}

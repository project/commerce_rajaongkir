<?php

namespace Drupal\commerce_rajaongkir_wahana;

interface WahanaRequestInterface {

  /**
   * Set the request configuration.
   *
   * @param array $configuration
   *   A configuration array from a CommerceShippingMethod.
   */
  public function setConfig(array $configuration);

}

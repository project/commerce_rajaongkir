<?php

namespace Drupal\commerce_rajaongkir_jnt;

use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\physical\WeightUnit;
use Drupal\commerce_rajaongkir\Rajaongakir;

/**
 * Class JNTRateRequest.
 *
 * @package Drupal\commerce_rajaongkir_jnt
 */
class JNTRateRequest extends JNTRequest {
  /**
   * @var \Drupal\commerce_shipping\Entity\ShipmentInterface*/
  protected $commerce_shipment;

  /**
   * @var array*/
  protected $configuration;

  /**
   * Set the shipment for rate requests.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $commerce_shipment
   *   A Drupal Commerce shipment entity.
   */
  public function setShipment(ShipmentInterface $commerce_shipment) {
    $this->commerce_shipment = $commerce_shipment;
  }

  /**
   * Fetch rates from the JNT API.
   */
  public function getRates(ShippingMethodInterface $shipping_method) {
    $config = \Drupal::config('commerce_rajaongkir.rajaongkirapikey');
    $api_key = $config->get('your_rajaongkir_api_key');

    $rajaongkir = new Rajaongakir($api_key);

    // Validate a commerce shipment has been provided.
    if (empty($this->commerce_shipment)) {
      throw new \Exception('Shipment not provided');
    }

    $rates = [];

    $address = $this->commerce_shipment->getShippingProfile()->get('address')->first();
    $store_address = $this->commerce_shipment->getOrder()->getStore()->getAddress();

    $total_weight = 0;
    foreach ($this->commerce_shipment->getItems() as $delta => $shipment_item) {
      $weight = $shipment_item->getWeight()->convert(WeightUnit::GRAM);
      $total_weight += $weight->getNumber();
    }

    if (!empty($address)) {

      $country_code = $address->getCountryCode();
      $city = $address->getLocality();

      $store_province = $store_address->getAdministrativeArea();
      $store_province_id = $rajaongkir->getProvinceCode($store_province);

      $store_city = $store_address->getLocality();
      $store_city_id = $rajaongkir->getCityCode($store_city, $store_province_id);


      if ($country_code == 'ID') {
        if ($city != '') {
          $province = $address->getAdministrativeArea();
          $province_id = $rajaongkir->getProvinceCode($province);
          $city_id = $rajaongkir->getCityCode($city, $province_id);
          $kecamatan = $address->getDependentLocality();
          $kecamatan_id = $rajaongkir->getKecamatanCode($kecamatan, $city_id);

          $client = \Drupal::httpClient();

          $request = $client->post("https://pro.rajaongkir.com/api/cost", [
            'form_params' => [
              'origin' => $store_city_id,
              'originType' => 'city',
              'destination' => $kecamatan_id,
              'destinationType' => 'subdistrict',
              'weight' => $total_weight,
              'courier' => 'jnt',
            ],
            'headers' => [
              'key' => $api_key,
            ]
          ]);

          $arrayResponse = json_decode($request->getBody(), TRUE);

          $costs = [];

          foreach ($arrayResponse['rajaongkir']['results'][0]['costs'] as $rate) {
            if ($rate['cost'][0]['etd'] == '1-1') {
              $rate['cost'][0]['etd'] = 1;
            }
            $costs[] = [
              'service' => $rate['service'],
              'value' => $rate['cost'][0]['value'],
              'etd' => $rate['cost'][0]['etd'],
            ];
          }

          usort($costs, function($a, $b) {
            return $b['value'] <=> $a['value'];
          });

          foreach ($costs as $rate) {
            $service_code = $rate['service'];

            // Only add the rate if this service is enabled.
            if (!in_array($service_code, $this->configuration['services'])) {
              continue;
            }

            $price = new Price((string) $rate['value'], 'IDR');
            $etd = '';
            if (!empty($rate['etd'])) {
              $etd = ' (Estimasi ' . $rate['etd'] . ' hari)';
            }
            $shipping_service = new ShippingService(
              'J&T - ' . $rate['service'],
              'J&T - ' . $rate['service'] . $etd
            );
            $rates[] = new ShippingRate([
              'shipping_method_id' => $shipping_method->id(),
              'service' => $shipping_service,
              'amount' => $price,
            ]);
          }
        }
      }
      else {
        $client = \Drupal::httpClient();

        $request = $client->post("https://pro.rajaongkir.com/api/v2/internationalCost", [
          'form_params' => [
            'origin' => $store_city_id,
            'destination' => $rajaongkir->getCountryCode($country_code),
            'weight' => $total_weight,
            'courier' => 'jnt',
          ],
          'headers' => [
            'key' => $api_key,
          ]
        ]);

        $arrayResponse = json_decode($request->getBody(), TRUE);

        $costs = [];

        $currency_rate = $arrayResponse['rajaongkir']['currency']['value'];

        foreach ($arrayResponse['rajaongkir']['results'][0]['costs'] as $rate) {
          if ($rate['etd'] == '1-1') {
            $rate['etd'] = 1;
          }
          $costs[] = [
            'service' => $rate['service'],
            'value' => ceil($rate['cost'] * $currency_rate),
            'etd' => $rate['etd'],
          ];
        }

        usort($costs, function($a, $b) {
          return $b['value'] <=> $a['value'];
        });

        foreach ($costs as $rate) {
          $service_code = $rate['service'];

          // Only add the rate if this service is enabled.
          if (!in_array($service_code, $this->configuration['services'])) {
            continue;
          }

          $price = new Price((string) $rate['value'], 'IDR');
          $etd = '';
          if (!empty($rate['etd'])) {
            $etd = ' (Estimasi ' . $rate['etd'] . ' hari)';
          }
          $shipping_service = new ShippingService(
            'J&T - ' . $rate['service'],
            'J&T - ' . $rate['service'] . $etd
          );
          $rates[] = new ShippingRate([
            'shipping_method_id' => $shipping_method->id(),
            'service' => $shipping_service,
            'amount' => $price,
          ]);
        }
      }
    }

    return $rates;
  }

}

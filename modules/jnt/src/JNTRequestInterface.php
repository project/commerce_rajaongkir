<?php

namespace Drupal\commerce_rajaongkir_jnt;

interface JNTRequestInterface {

  /**
   * Set the request configuration.
   *
   * @param array $configuration
   *   A configuration array from a CommerceShippingMethod.
   */
  public function setConfig(array $configuration);

}
